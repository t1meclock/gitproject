import math
from xml.etree.ElementInclude import include

def Calculator():
    try:
        a, b = (float(input("Введите 2 числа: "))for i in range(2))
        operations = ["1. * - Умножение" "\n2. + - Сложение" "\n3. - - Вычитание" "\n4. / - Деление" "\n5. // - Целочисленное деление" "\n6. ** - Возведение в степень" "\n7. % - Остаток от деления" "\n8. S - Площадь" "\n9. P - Периметр" "\n10. Copysign - Возвращает модуль числа" "\n11. Atan - Арктангенс" "\n12. Hypot - Гипотенуза" "\n13. Fmod - Остаток"]
        for i in range(len(operations)):
            print(operations[i], end=", ")
        op = input("\nВыберите задание: ")
        match op:
            case "*":
                print("a * b = ", a*b)
            case "/":
                print("a / b = ", a/b)
            case "+":
                print("a + b = ", a+b)
            case "-":
                print("a - b = ", a-b)
            case "//":
                print("a // b = ", a//b)
            case "**":
                print("a ** b = ", a**b)
            case "%":
                print("a % b = ", a % b)
            case "P":
                c = float(input("Введите третью сторону: "))
                print("Периметр равен: ", 4*a+4*b+4*c)
            case "S":
                c = float(input("Введите третью сторону: "))
                print("Площадь равна: ", 2*(a*b+b*c+c*a))
            case "C":
                print("Copysign = ", math.copysign(a,b))
            case "Atan":
                print("Арктангенс = ", math.atan2(a,b))
            case "Hypot":
                print("Гипотенуза = ", math.hypot(a,b))
            case "Fmod":
                print("Остаток = ", math.fmod(a,b))
    except Exception:
                print("Ошибка.")

def String():
    try:
        text=input("Введите строку: ")
        print(f"Длина строки: ",len(text))
        print("Количество пробелов: ", text.count(" "))
        print("Количество запятых: ", text.count(","))
    except Exception:
        print("Ошибка.")


def Matrix(a,b,c,d):
    try:
            matrix = []
            for i in range(b):
                matrix.append([])
                for j in range(a):
                    matrix[i].append(c)
                    c += d
            for i in range(0, b):
                for j in range(0, a):
                    print(matrix[i][j], end=' ')
                print(" ")
    except Exception:
        print("Ошибка.")

while True:
        try:
            turner = int(input("Введите задание 1 - 3 \n0 - Выход: "))
            match turner:
                case 1:
                    Calculator()
                case 2:
                    String()
                case 3:
                    a = int(input("Количество столбцов в матрице: "))
                    b = int(input("Количество строк в матрице: "))
                    c = int(input("Начало: "))
                    d = int(input("Шаг: "))
                    Matrix(a,b,c,d)
                case 0:
                    break
        except Exception:
                    print("Ошибка.")
